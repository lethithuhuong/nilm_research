% Decision Tree model 
clc; close all; clear; 

%% CODE FOR THE FIRST SCENARIO 

%Reading data 
%case1
data = readtable("./dataset/Steady_State_Feature1.csv"); 
%case2
% data = readtable("./Train_Model_Steady/Steady_State_Feature2.csv"); 
%case3
% data = readtable("./Train_Model_Steady/Steady_State_Feature3.csv"); 

% Build DT model for training  
%case1 
predictorNames = {'Fundamental','First_Harmonic','Second_Harmonic','Third_Harmonic','Fouth_Harmonic','Fifth_Harmonic','THD'};
%case2 
% predictorNames = {'magnitude60', 'magnitude180', 'magnitude300', 'magnitude420', 'phase60', 'phase180', 'phase300', 'phase420'};
%case3 
% predictorNames = {'PSD_first','PSD_third','PSD_fifth','PSD_seventh'};

predictors = data(:, predictorNames);
response = data.label;

% Train a classifier
DT = fitctree(predictors,response,'ClassNames', ...
              categorical({'Fan'; 'Kettle';  'SS monitor'; 'LG monitor'; 'Hairdryer'}));
% Perform cross-validation
partitionedModel = crossval(DT, 'KFold', 5);

% Compute validation predictions
validationPredictions = kfoldPredict(partitionedModel);

% Compute validation accuracy
validationAccuracy = 1 - kfoldLoss(partitionedModel, 'LossFun', 'ClassifError');
disp("Accuracy for validation data=" + validationAccuracy); 

% Evaluation for training process  
t =response'; 
targets = categorical(t); 
outputs = validationPredictions'; 

figure; 
plotconfusion(targets,outputs)

%% CODING FOR THE SECOND SCENARIO 

%Decision Tree model 
clc; close all; clear; 

%Split data in 2 parts: train and test data
data = readtable("./dataset/DB_Feature2.csv"); 
% Divide 70% for traning and validation data ; 30% for test data as randomly 
[m,n] = size (data); 
P = 0.7; 
idx = randperm(m)  ; % indexing for random position data 

Training = data(idx(1:round(P*m)),:) ; 
Testing = data(idx(round(P*m)+1:end),:) ;

%saving data in csv file for train and test 
writetable (Training,'./dataset/TrainDB_Feature2.csv'); 
writetable(Testing,'./dataset/TestDB_Feature2.csv'); 

dbTrainData = readtable('./dataset/TrainDB_Feature2.csv'); 
dbTestData =readtable ('./dataset/TestDB_Feature2.csv'); 

%Build DT model for train data 
predictorNames = {'magnitude60', 'magnitude180', 'magnitude300', 'magnitude420', 'phase60', 'phase180', 'phase300', 'phase420'};
predictors = dbTrainData(:, predictorNames);
response = dbTrainData.label;

% Train a classifier
DT = fitctree(predictors,response,...
              'ClassNames', categorical({'Fan'; 'Fan Kettle'; 'Fan SS Kettle'; 'Kettle'; 'SS Fan'; 'SS Kettle'; 'SS monitor'}));

% Perform cross-validation
partitionedModel = crossval(DT, 'KFold', 5);

% Compute validation predictions
validationPredictions = kfoldPredict(partitionedModel);

% Compute validation accuracy
validationAccuracy = 1 - kfoldLoss(partitionedModel, 'LossFun', 'ClassifError');

% Evaluation for training process 
t =response'; 
targets = categorical(t); 
outputs = validationPredictions'; 
figure; 
plotconfusion(targets,outputs)

% Evaluation for testing process 
testPredictors = dbTestData(:, predictorNames);
testResponse = dbTestData.label;

modelPredictOutput= predict(DT,testPredictors); 

targetTesting = categorical(testResponse)'; 
outputTesting = modelPredictOutput'; 

figure; 
title ("Testing process");
plotconfusion(targetTesting,outputTesting)

