close all; clear ; clc;
% target load 
targetLoad = readmatrix ("./SingleLoad_new/Fan_16s_1.dat"); 
targetLoad_Actual1 = downsample(targetLoad,8);

% target load 
targetLoad = readmatrix ("./SingleLoad_new/Hairdryer_16s_1.dat"); 
targetLoad_Actual2 = downsample(targetLoad,8);

% target load 
targetLoad = readmatrix ("./SingleLoad_new/Kettle_16s_1.dat"); 
targetLoad_Actual3 = downsample(targetLoad,8);

% target load 
targetLoad = readmatrix ("./SingleLoad_new/LG_Monitor_16s_1.dat"); 
targetLoad_Actual4 = downsample(targetLoad,8);

% target load 
targetLoad = readmatrix ("./SingleLoad_new/SS_monitor_16s_1.dat"); 
targetLoad_Actual5 = downsample(targetLoad,8);
%% case 1
[harPowApp1,THDApp1] =getHarmonicPower(targetLoad_Actual1); 
[harPowApp2,THDApp2] =getHarmonicPower(targetLoad_Actual2); 
[harPowApp3,THDApp3] =getHarmonicPower(targetLoad_Actual3); 
[harPowApp4,THDApp4] =getHarmonicPower(targetLoad_Actual4); 
[harPowApp5,THDApp5] =getHarmonicPower(targetLoad_Actual5); 

featureApp1= [harPowApp1 THDApp1]; 
featureApp2= [harPowApp2 THDApp2]; 
featureApp3= [harPowApp3 THDApp3]; 
featureApp4= [harPowApp4 THDApp4]; 
featureApp5= [harPowApp5 THDApp5]; 
%% case 2: FFT  
signal = targetLoad_Actual1(14481:end); 
figure;
plot (signal); 

% steadySignal = signal(14481:14481+2000);
% hammingWindow = hamming(2000); 
% steadySignal = steadySignal.*hammingWindow;
% figure;
% plot (steadySignal); 

[maglitude_60, phase_60]= getMagnitudePhase(signal,60,1); 
[maglitude_180,phase_180]= getMagnitudePhase(signal,180,1); 
[maglitude_300,phase_300]= getMagnitudePhase(signal,300,1); 
[maglitude_420,phase_420]= getMagnitudePhase(signal,420,1); 

array_feature = [maglitude_60 maglitude_180 maglitude_300 maglitude_420 phase_60 phase_180 phase_300 phase_420]; 
%% case 3  
fsamp = 2000;
Tsamp = 1/fsamp;
x= targetLoad_Actual1(14481:end); 
L = length (x); 
T= 1/fsamp; 
t= (0:L-1)*T; 

psd60  = getPSD(x,60, 7);
psd180 = getPSD(x,180, 7);
psd300 = getPSD(x,300, 7);
psd420 = getPSD(x,420, 7); 

psd = [psd60 psd180 psd300 psd420]; 
%%
%function case1
function [harmonicPower,THD] = getHarmonicPower (data)
    fs =2000; 
    [r,hPower,~] = thd(data,fs); 
    harmonicPower =hPower'; 
    THD=r; 
    %disp(hPower); 
end 

%function case2 
function [magnitude, phase] = getMagnitudePhase(data, freq,sd_index)
    fsamp = 2000; 
    L = length(data); 
    Fs = linspace(0,fsamp,L);
    fft_data = fft(data); 
    amp= abs(fft_data); 
    pha = angle(fft_data); 
    i = find(Fs<=freq+sd_index & Fs>=freq-sd_index);
%   index=max(i); 
    disp (i); 
    magnitude = amp(i); 
    phase = pha(i); 
end 

%function case3
function psd = getPSD(data,Fs,sd_index)
    fsamp = 2000;
    % Choose FFT size and calculate spectrum
    Nfft = 1024;
    [Pxx,freq] = pwelch(data,gausswin(Nfft),Nfft/2,Nfft,fsamp);
    i = freq<=Fs+sd_index & freq>=Fs-sd_index;
%   disp(i); 
    psd = Pxx(i);
end
